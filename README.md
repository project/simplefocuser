#CONTENTS OF THIS FILE#

- Introduction
- Requirements
- Recommended modules
- Installation
- Configuration
- Troubleshooting
- FAQ
- Maintainers

##INTRODUCTION##

Textfield SimpleFocuser is a module that does one thing: on all pages, it finds
and changes focus to the first text field it finds. That's it. Simple.

For a full description of the module, visit the project page:
https://www.drupal.org/sandbox/elliotc/2587591

To submit bug reports and feature suggestions, or to track changes:
https://www.drupal.org/project/issues/2587591

##REQUIREMENTS##

No special requirements

##RECOMMENDED MODULES##

No other modules are required. However, this was tested using
Omega Theme 7.x-3.1
https://www.drupal.org/project/omega

It's possible that other themes could create some unintended consequences.

##INSTALLATION##

Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

##CONFIGURATION##

The module has no menu or modifiable settings. There is no configuration. When
enabled, it will always set the focus to the first form textfield on the page.

##TROUBLESHOOTING##

The "first" element on the page might not be the first in the HTML.

##FAQ##

Q: What if I want to select some other element for the focus?
A: Currently, that won't happen in this module. You'll need to use something
   else or write your own Javascript. As you can see, the jQuery in this
   module is pretty simple.

Q: Can I turn it off on some pages?
A: Currently, no. It's going to select the form textfield on each and every
   page whether you want it to or not.

##MAINTAINERS##

Current maintainers:

- Elliot Christenson (elliotc) - https://www.drupal.org/u/elliotc
